﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA_calc
{
    public partial class FormMain : Form
    {
        private double firstNum, secondNum;
        private string action;
        public int sizeChange;
        public int calcChanged = 1;
        public double coef = 1;
        public double coef2 = 1;



        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "1";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "1";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "2";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "2";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "3";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "3";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "4";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "4";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "5";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "5";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "6";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "6";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "7";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "7";
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "8";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "8";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "9";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "9";
            }
        }

        private void button0_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                textBoxNumber.Text += "0";
            }
            else if (calcChanged == 2)
            {
                textBoxWeight.Text += "0";
            }
        }

        private void buttonDot_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                if (!textBoxNumber.Text.Contains(','))
                {
                    textBoxNumber.Text += ",";
                }
            }
            else if (calcChanged==2)
            {
                if (!textBoxWeight.Text.Contains(','))
                {
                    textBoxWeight.Text += ",";
                }
            }
            
        }

        private void buttonErase_Click(object sender, EventArgs e)
        {
            textBoxNumber.Clear();
        }

        private void buttonCalc_Click(object sender, EventArgs e)
        {
            try
            {
                secondNum = double.Parse(textBoxNumber.Text);
                switch (action)
                {
                    case "+":
                        textBoxNumber.Text = (firstNum + secondNum).ToString();
                        break;
                    case "-":
                        textBoxNumber.Text = (firstNum - secondNum).ToString();
                        break;
                    case "*":
                        textBoxNumber.Text = (firstNum * secondNum).ToString();
                        break;
                    case "/":
                        textBoxNumber.Text = (firstNum / secondNum).ToString();
                        break;
                }
                string record = firstNum + " " + action + " " + secondNum + " = " + textBoxNumber.Text;
                listBoxHystory.Items.Add(record);
            }
            catch
            {
                MessageBox.Show("Error!");
                textBoxNumber.SelectAll();
            }
        }

        private void buttonPlusMinus_Click(object sender, EventArgs e)
        {
            if (textBoxNumber.Text.Length > 0)
            {
                if (textBoxNumber.Text[0] == '-')
                {
                    textBoxNumber.Text = textBoxNumber.Text.Remove(0, 1);
                }
                else
                {
                    textBoxNumber.Text = textBoxNumber.Text.Insert(0, "-");
                }
            }
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            if (calcChanged == 1)
            {
                if (textBoxNumber.Text.Length > 0)
                {
                    textBoxNumber.Text = textBoxNumber.Text.Substring(0, textBoxNumber.Text.Length - 1);
                }
            }
            else if (calcChanged == 2)
            {
                if (textBoxWeight.Text.Length > 0)
                {
                    textBoxWeight.Text = textBoxWeight.Text.Substring(0, textBoxWeight.Text.Length - 1);
                }
            }
        }

        private void buttonMult_Click(object sender, EventArgs e)
        {
            try
            {
                firstNum = double.Parse(textBoxNumber.Text);
                textBoxNumber.Clear();
                action = "*";
            }
            catch
            {
                MessageBox.Show("Error!");
                textBoxNumber.SelectAll();
            }
        }

        private void buttonDev_Click(object sender, EventArgs e)
        {
            try
            {
                firstNum = double.Parse(textBoxNumber.Text);
                textBoxNumber.Clear();
                action = "/";
            }
            catch
            {
                MessageBox.Show("Error!");
                textBoxNumber.SelectAll();
            }
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            try
            {
                firstNum = double.Parse(textBoxNumber.Text);
                textBoxNumber.Clear();
                action = "+";
            }
            catch
            {
                MessageBox.Show("Error!");
                textBoxNumber.SelectAll();
            }
        }

        private void buttonMinus_Click(object sender, EventArgs e)
        {
            try
            {
                firstNum = double.Parse(textBoxNumber.Text);
                textBoxNumber.Clear();
                action = "-";
            }
            catch
            {
                MessageBox.Show("Error!");
                textBoxNumber.SelectAll();
            }
        }

        private void buttonClearHistory_Click(object sender, EventArgs e)
        {
            listBoxHystory.Items.Clear();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Width = 600;
            button10.Visible = false;
            button11.Visible = true;
            panel1.Visible = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.Width = 475;
            button10.Visible = true;
            button11.Visible = false;
            panel1.Visible = false;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            this.Width = 475;
            textBoxWeight.Visible = false;
            radioButtonMath.Checked = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            calcChanged = 1;
            textBoxNumber.Visible = true;
            label1.Visible = true;
            listBoxHystory.Visible = true;
            buttonClearHistory.Visible = true;
            tableLayoutButtons.Visible = true;
            textBoxWeight.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
            label2.Visible = false;
            buttonWeightCalc.Visible = false;
            textBoxWeightResult.Visible = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            calcChanged = 2;
            textBoxNumber.Visible = false;
            label1.Visible = false;
            listBoxHystory.Visible = false;
            buttonClearHistory.Visible = false;
            tableLayoutButtons.Visible = false;
            textBoxWeight.Visible = true;
            panel2.Visible = true;
            panel3.Visible = true;
            label2.Visible = true;
            buttonWeightCalc.Visible = true;
            textBoxWeightResult.Visible = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            calcChanged = 0;
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void textBoxNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonWeightCalc_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxWeightResult.Clear();
                textBoxWeightResult.Text += (double.Parse(textBoxWeight.Text)*coef/coef2).ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте правильность заполнения полей!");
                textBoxNumber.SelectAll();
            }
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            coef = 1000;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 1000;
        }

        private void radioButton3_CheckedChanged_1(object sender, EventArgs e)
        {
            coef = 0.001;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 0.001;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            coef = 6.35;
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 6.35;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            coef = 1/2.20462;
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 1/2.20462;
        }

        private void radioButton11_CheckedChanged(object sender, EventArgs e)
        {
            coef = 1/35.274;
        }

        private void radioButton12_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 1/35.274;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            coef2 = 1;
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e)
        {
            coef = 1;
        }

        private void listBoxHystory_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxHystory.Items.Clear();
        }
    }
}
